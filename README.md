# Projet Docker

## Sujet : Mise en place d'un serveur Nextcloud

### Services mis en place

* Nextcloud
* MariaDB
* Prometheus
* Grafana
* Portainer

### Les fichiers

Voici les différents fichiers utilisés pour mettre en place la stack :
<ul>
    <li> <a href="files/docker-compose.yml"> Docker compose</a></li>
    <li> <a href="files/DockerfileMariadb"> Dockerfile pour MariaDB</a> </li>
        <ul>
            <li> <a href="files/scriptdb.sh"> Script ENTRYPOINT pour MariaDB</a> </li>
        </ul>
    <li> <a href="files/DockerfileNextcloud"> Dockerfile pour Nextcloud</a> </li>
        <ul>
            <li> <a href="files/scriptnextcloud.sh"> Script ENTRYPOINT pour Nextcloud</a> </li>
        </ul>
    <li> <a href="files/DockerfilePrometheus"> Dockerfile pour Prometheus</a></li>
        <ul>
            <li> <a href="files/scriptprometheus.sh"> Script ENTRYPOINT pour Prometheus</a> </li>
            <li> <a href="files/prometheus.yml">Fichier de configuration prometheus.yml</a></li>
            <li> <a href="files/dashboard_grafana.yml">Fichier grafana pour les dashboards</a></li>
        </ul>
</ul>

### Mise en place d'images personnalisées

Pour mettre en place, j'ai créé des images customs pour répondre aux besoins de la stack mise en place :

**Nextcloud** : J'ai créé un Dockerfile, je suis parti de l'image de base de Nextcloud, j'ai installé le service Prometheus pour le monitoring, j'ai copié un script basé sur l'entrypoint présent initialement dans l'image et j'ai ajouté une ligne permettant de lancer le service Prometheus.

```Dockerfile
FROM nextcloud

RUN apt update && apt install -y prometheus

COPY scriptnextcloud.sh /init.sh

RUN chmod +x /init.sh

ENTRYPOINT ["/init.sh"]

CMD ["apache2-foreground"]
```

**MariaDB** : J'ai créé un Dockerfile, je suis parti de l'image de base de MariaDB, j'ai installé le service Prometheus pour le monitoring, j'ai copié un script basé sur l'entrypoint présent initialement dans l'image et j'ai ajouté une ligne permettant de lancer le service Prometheus.

```Dockerfile
FROM mariadb:10.6

RUN apt update && apt install -y prometheus 

COPY scriptdb.sh /init.sh

RUN chmod +x /init.sh

ENTRYPOINT ["/init.sh"]
```

**Prometheus** : J'ai créé un Dockerfile pour installer manuellement le service Prometheus pour comprendre les différentes étapes d'installation. Je suis parti de l'image de Debian, j'ai copié un script qui s'occupe de l'installation du service à l'aide de la fonction `entrypoint`.

```Dockerfile
FROM debian

COPY scriptprometheus.sh /entrypoint.sh

COPY prometheus.yml /prometheus.yml

EXPOSE 9090

ENTRYPOINT [ "/entrypoint.sh" ]
```

### Mise en place avec Docker compose

Voici le fichier `docker-compose.yml` pour lancer la stack :

```yml
version: "3.7"

services:
  db:
    image: clem1108/mariadb_custom
    restart: always
    command: --transaction-isolation=READ-COMMITTED --log-bin=binlog --binlog-format=ROW
    ports:
      - 9090
      - 9100
    volumes:
      - db:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=root
      - MYSQL_PASSWORD=root
      - MYSQL_DATABASE=nextcloud
      - MYSQL_USER=nextcloud

  app:
    image: clem1108/nextcloud_custom
    restart: always
    ports:
      - 8080:80
      - 9090
      - 9100
    links:
      - db
    volumes:
      - nextcloud:/var/www/html
    command: apache2-foreground
    environment:
      - MYSQL_PASSWORD=root
      - MYSQL_DATABASE=nextcloud
      - MYSQL_USER=nextcloud
      - MYSQL_HOST=db

  prometheus:
    image: clem1108/prometheus_custom
    restart: always
    ports:
      - 9090:9090
    healthcheck:
      test: [ "CMD", "curl", "-f", "http://localhost:9090" ]
      interval: 10s
      timeout: 5s
      retries: 10

  grafana:
    image: grafana/grafana
    container_name: grafana
    volumes:
      - grafana_data:/var/lib/grafana
    environment:
      - GF_SECURITY_ADMIN_USER=root
      - GF_SECURITY_ADMIN_PASSWORD=root
      - GF_USERS_ALLOW_SIGN_UP=false
    restart: unless-stopped
    ports:
      - 3000:3000
    labels:
      org.label-schema.group: "monitoring"
    depends_on:
      prometheus:
        condition: service_healthy

  portainer:
    image: portainer/portainer-ce:latest
    ports:
      - 9443:9443
    volumes:
      - data:/data
      - /var/run/docker.sock:/var/run/docker.sock
    restart: unless-stopped

volumes:
  db:
  nextcloud:
  grafana_data:
  data:
```

### Mise en place tableau de bord personnalisé

J'ai créé un tableau de bord personnalisé qui permet de visualiser la quantité de RAM disponible, l'espace disque disponible, la quantité de SWAP utilisé, l'utilisation du CPU.

<a href="files/dashboard_grafana.json"> Dashboard personnalisé</a></li>

### Difficultés rencontrées

J'ai rencontré plusieurs difficultés :

* Sur le node `Nextcloud` : Le service ne voulait pas se lancer car il ne trouvait pas de valeur à la valeur `$1` --> La solution a été de rajouter la ligne `CMD ["apache2-foreground"]` et de modifier la typologie sur la ligne `ENTRYPOINT ["/init.sh"]` que je n'avais pas mis entre crochet.
* Sur le node `MariaDB` : Le script se lançait avec l'utilisateur `root`, puis MariaDB est paramétré pour relancer le script avec l'utilisateur `mysql` si celui-ci n'est pas utilisé par défaut. Sauf que `mysql` n'a pas les droits pour lancer le service `prometheus` donc l'exécution du script s'interrompait --> la solution a été de mettre une vérification avant l'exécution de la commande `service prometheus start` : si l'utilisateur est `root`, alors la commande est exécutée, sinon il passe et exécute le reste du script, donc `MariaDB`.
* L'installation du service `node_exporter` : Afin de pouvoir récupérer les métriques matérielles (RAM, espace disque, ...), il est nécessaire d'installer le service `node_exporter`. J'ai donc ajouté les lignes d'installation dans les entrypoints des différents containers. Le service `node_exporter` ne peu têtre lancé qu'au premier plan, donc son lancement stoppait les étapes de lancement. J'ai donc redirigé tous les flux vers `/dev/null` afin que je puisse récu^érer la main sur le terminal, et donc pouvoir continuer le lancement des containers.
