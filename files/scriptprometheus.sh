#!/bin/sh

apt update && apt install wget curl systemd -y

groupadd --system prometheus

useradd -s /sbin/nologin --system -g prometheus prometheus

mkdir /etc/prometheus && mkdir /var/lib/prometheus

curl -s https://api.github.com/repos/prometheus/prometheus/releases/latest | grep browser_download_url | grep linux-amd64 | cut -d '"' -f 4 | wget -qi -

tar -xvf prometheus-*.*.*.linux-amd64.tar.gz

mv prometheus-*.*.*.linux-amd64/* /etc/prometheus/

chown prometheus:prometheus /etc/prometheus && chown prometheus:prometheus /var/lib/prometheus

chown -R prometheus:prometheus /etc/prometheus/consoles && chown -R prometheus:prometheus /etc/prometheus/console_libraries

cp /etc/prometheus/prometheus /usr/local/bin/ && cp /etc/prometheus/promtool /usr/local/bin/

cp /prometheus.yml /etc/prometheus/prometheus.yml

/usr/local/bin/prometheus \
--config.file /etc/prometheus/prometheus.yml \
--storage.tsdb.path /var/lib/prometheus/ \
--web.console.templates=/etc/prometheus/consoles \
--web.console.libraries=/etc/prometheus/console_libraries


